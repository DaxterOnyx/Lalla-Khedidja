void setup() {
 Serial.begin(9600);
 pinMode(A0, INPUT);
}

int Gap = 50;
int PositivScale = 50;
int NegativScale = 400;

int avgPos = -1;
int decelerate = -1;
int accelerate = -1;

void loop() {
  int input =  analogRead(A0);
  int output = -666;

  if(avgPos == -1 && input > 10)
  {
    avgPos = input;
    decelerate = input - Gap;
    accelerate = input + Gap;
  }

  if(input < decelerate)
  {
    output = -100;
    output = (((input - decelerate) * -100) / NegativScale)*-1;
  }else if(input > accelerate)
  {
    output = 100;
    output = ((input - accelerate) * 100) / PositivScale;    
  }else
  {
    output = 0;
  }

  
  Serial.println(input);
  //Serial.print("->");
  //Serial.println(output);
}
