//ARDIUNO LALLA KHEDIDJA

//PARAMETERS

int ledPin = 12;

//Direction
//Left
int leftPin = A0;
//Rigt
int rightPin = A1;
//Acceleration
int accPinLeft = A2;
int accPinRight = A3;
//Spells
//Vegetal
int VegetalPin = A4;
//Light
int LightPin = A5;

void setup()
{
  // setup pour la fonction makey makey ----------------------------------------------------------------------------------------------------------------------------

  Serial.begin(9600); /* définie le taux de transfert de donnée à 9600 bauds. */

  Serial.println("Initialisation");

  // Acceleration
  pinMode(accPinLeft, INPUT);
  pinMode(accPinRight, INPUT);

  //setup Direction
  pinMode(leftPin, INPUT);
  pinMode(rightPin, INPUT);

  //setup Spells
  pinMode(VegetalPin, INPUT);
  pinMode(LightPin, INPUT);
}
void loop()
{
  delay(100);
  int conductivite_Vegetal = analogRead(VegetalPin);
  int conductivite_Light = analogRead(LightPin);
  
  //Direction INPUT
  //LEFT
  int dirLeft = analogRead(leftPin);

  //RIGHT
  int dirRight = analogRead(rightPin);

  //Acceleration
  int accLeft = analogRead(accPinLeft);
  int accRight = analogRead(accPinRight);
  int acc = (accLeft + accRight) / 2;
  
  String output = String(conductivite_Vegetal) + " " + String(conductivite_Light) + " " + String(dirLeft) + " " + String(dirRight) + " " + String(acc);

  Serial.println(output);
}
