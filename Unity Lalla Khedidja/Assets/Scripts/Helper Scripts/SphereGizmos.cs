using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereGizmos : MonoBehaviour
{
    public bool Show = true;
    public float Size = 0.2f;
    public Color Color = Color.red;
    private void OnDrawGizmos()
    {
        Gizmos.color = Color;
        Gizmos.DrawWireSphere(transform.position,Size);
    }
}
