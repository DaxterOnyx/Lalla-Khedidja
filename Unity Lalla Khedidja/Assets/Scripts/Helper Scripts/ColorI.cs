using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public struct ColorI {
	#region Constants
	public static readonly ColorI white = new ColorI(255, 255, 255, 255);
	public static readonly ColorI black = new ColorI(0, 0, 0, 255);

	public static readonly ColorI red = new ColorI(255, 0, 0, 255);
	public static readonly ColorI green = new ColorI(0, 255, 0, 255);
	public static readonly ColorI blue = new ColorI(0, 0, 255, 255);

	public static readonly ColorI magenta = new ColorI(255, 0, 255, 255);
	public static readonly ColorI yellow = new ColorI(255, 255, 0, 255);
	public static readonly ColorI cyan = new ColorI(0, 255, 255, 255);

	public static readonly ColorI orange = new ColorI(255, 127, 0, 255);
	public static readonly ColorI brown = new ColorI(64, 32, 0, 255);

	public static readonly ColorI invisible = new ColorI(0, 0, 0, 0);
	#endregion
	
	public byte r,g,b,a;
	public byte this[int index] {
		get {
			if(index == 0) { return r; }
			if(index == 1) { return g; }
			if(index == 2) { return b; }
			if(index == 3) { return a; }

			return 0;
		}
		set {
			if(index == 0) { r = value; }
			if(index == 1) { g = value; }
			if(index == 2) { b = value; }
			if(index == 4) { a = value; }
		}
	}

	public ColorI(byte r, byte g, byte b, byte a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}
	public ColorI(byte r, byte g, byte b) {
		this.r = r;
		this.g = g;
		this.b = b;
		a = 255;
	}
	public ColorI(float r, float g, float b, float a) {
		this.r = (byte)r;
		this.g = (byte)g;
		this.b = (byte)b;
		this.a = (byte)a;
	}
	public ColorI(float r, float g, float b) {
		this.r = (byte)r;
		this.g = (byte)g;
		this.b = (byte)b;
		a = 255;
	}
	public ColorI(ColorI color) {
		this.r = color.r;
		this.g = color.g;
		this.b = color.b;
		this.a = color.a;
	}
	public ColorI(Color color) {
		this.r = (byte)(color.r * 255);
		this.g = (byte)(color.g * 255);
		this.b = (byte)(color.b * 255);
		this.a = (byte)(color.a * 255);
	}
	public ColorI(Vector4 vector) {
		this.r = (byte)vector.x;
		this.g = (byte)vector.y;
		this.b = (byte)vector.z;
		this.a = (byte)vector.w;
	}

	public Color ToFloat() {
		return new Color(r / 255f, g / 255f, b / 255f, a / 255f);
	}

	#region Advanced
	public static ColorI operator -(ColorI a) {
		return new ColorI(255 - a.r, 255 - a.g, 255 - a.b, 255 - a.a);
	}
	public static ColorI operator -(ColorI a, ColorI b) {
		return new ColorI(a.r - b.r, a.g - b.g, a.b - b.b, a.a - b.r);
	}

	public static bool operator !=(ColorI lhs, ColorI rhs) {
		return lhs.r != rhs.r && lhs.g != rhs.g && lhs.b != rhs.b && lhs.a != rhs.a;
	}

	public static ColorI operator *(float d, ColorI a) {
		return new ColorI(d * a.r, d * a.g, d * a.b, d * a.a);
	}
	public static ColorI operator *(ColorI a, float d) {
		return new ColorI(a.r * d, a.g * d, a.b * d, a.a * d);
	}
	public static ColorI operator /(ColorI a, float d) {
		return new ColorI(a.r / d, a.g / d, a.b / d, a.a / d);
	}
	public static float operator /(float d, ColorI a) {
		d /= a.r; d /= a.g; d /= a.b; d /= a.a;
		return d;
	}
	public static ColorI operator *(ColorI a, ColorI b) {
		return new ColorI(a.r * b.r, a.g * b.g, a.b * b.b, a.a * b.a);
	}
	public static ColorI operator /(ColorI a, ColorI b) {
		return new ColorI(a.r / b.r, a.g / b.g, a.b / b.b, a.a / b.a);
	}
	public static ColorI operator +(ColorI a, ColorI b) {
		return new ColorI(a.r + b.r, a.g + b.g, a.b + b.b, a.a + b.a);
	}

	public static bool operator ==(ColorI lhs, ColorI rhs) {
		return lhs.r == rhs.r && lhs.g == rhs.g && lhs.b == rhs.b && lhs.a == rhs.a;
	}

	public static implicit operator ColorI(Color c) {
		return new ColorI(c);
	}
	public static implicit operator Color(ColorI c) {
		return c.ToFloat();
	}

	/*public static implicit operator ColorI(Vector2 v) {
		return new ColorI(v.x, v.y, 0);
	}
	public static implicit operator ColorI(Vector3 v) {
		return new ColorI(v);
	}
	public static implicit operator ColorI(Vector4 v) {
		return new ColorI(v.x, v.y, v.z);
	}
	public static implicit operator Vector2(ColorI v) {
		return new Vector3(v.x, v.y, v.z);
	}
	public static implicit operator Vector3(ColorI v) {
		return new Vector3(v.x, v.y, v.z);
	}
	public static implicit operator Vector4(ColorI v) {
		return new Vector3(v.x, v.y, v.z);
	}*/

	public override bool Equals(object obj) {
		if(obj.GetType() == typeof(ColorI)) {
			ColorI c = (ColorI)obj;
			return this.r == c.r && this.g == c.g && this.b == c.b && this.a == c.a;
		}

		return base.Equals(obj);
	}
	public override int GetHashCode() {
		return base.GetHashCode();
	}
	public override string ToString() {
		return "(" + r + ", " + g + ", " + b + ", " + a + ")";
	}
	#endregion
}
