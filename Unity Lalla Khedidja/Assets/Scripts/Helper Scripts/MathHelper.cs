using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public static class MathHelper {

	public static VectorI3 ArrayToDirection(this Vector3 arrayPosition, Vector3 arraySize) {
		if(arrayPosition.x == 0) { arrayPosition.x = -1; }
		else if(arrayPosition.x == arraySize.x - 1) { arrayPosition.x = 1; }
		else { arrayPosition.x = 0; }

		if(arrayPosition.y == 0) { arrayPosition.y = -1; }
		else if(arrayPosition.y == arraySize.y - 1) { arrayPosition.y = 1; }
		else { arrayPosition.y = 0; }

		if(arrayPosition.z == 0) { arrayPosition.z = -1; }
		else if(arrayPosition.z == arraySize.z - 1) { arrayPosition.z = 1; }
		else { arrayPosition.z = 0; }

		return arrayPosition;
	}

	public static bool IsEdgeOfArray(this VectorI3 arrayPosition, VectorI3 arraySize) {
		arraySize -= VectorI3.one;
		return (arrayPosition.x == 0 || arrayPosition.x == arraySize.x || arrayPosition.y == 0 || arrayPosition.y == arraySize.y || arrayPosition.z == 0 || arrayPosition.z == arraySize.z);
	}
	public static bool IsCornerEdgeOfArray(this VectorI3 arrayPosition, VectorI3 arraySize) {
		if(arrayPosition.x == 0 || arrayPosition.x == arraySize.x - 1) {
			if(arrayPosition.y == 0 || arrayPosition.y == arraySize.y - 1) { return true; }
			else if(arrayPosition.z == 0 || arrayPosition.z == arraySize.z - 1) { return true; }
		}

		if(arrayPosition.y == 0 || arrayPosition.y == arraySize.y - 1) {
			if(arrayPosition.x == 0 || arrayPosition.x == arraySize.x - 1) { return true; }
			else if(arrayPosition.z == 0 || arrayPosition.z == arraySize.z - 1) { return true; }
		}

		if(arrayPosition.z == 0 || arrayPosition.z == arraySize.z - 1) {
			if(arrayPosition.x == 0 || arrayPosition.x == arraySize.x - 1) { return true; }
			else if(arrayPosition.y == 0 || arrayPosition.y == arraySize.y - 1) { return true; }
		}

		return false;
	}
	public static bool IsCornerOfArray(this VectorI3 arrayPosition, VectorI3 arraySize) {
		return
			((arrayPosition.x == 0) || (arrayPosition.x == arraySize.x - 1)) &&
				((arrayPosition.y == 0) || (arrayPosition.y == arraySize.y - 1)) &&
				((arrayPosition.z == 0) || (arrayPosition.z == arraySize.z - 1));
	}

	public static bool ArrayOutOfBounds(this VectorI3 index, VectorI3 arraySize) {
		//Debug.LogWarning("ArrayOutOfBounds("+index+", "+arraySize+") x = " + (index.x > arraySize.x - 1 || index.x < 0) + " y = " + (index.y > arraySize.y - 1 || index.y < 0) + " z = " + (index.z > arraySize.z - 1 || index.z < 0));
		if((int)index.x > ((int)arraySize.x - 1) || (int)index.x < 0) { return true; }
		if((int)index.y > ((int)arraySize.y - 1) || (int)index.y < 0) { return true; }
		if((int)index.z > ((int)arraySize.z - 1) || (int)index.z < 0) { return true; }

		return false;
	}

	public static VectorI3 Wrap3DIndex(this VectorI3 positionIndex, VectorI3 arraySize) {
		VectorI3 newDirection = new VectorI3(
			positionIndex.x % arraySize.x,
			positionIndex.y % arraySize.y,
			positionIndex.z % arraySize.z
			);

		if(newDirection.x < 0) { newDirection.x = arraySize.x + newDirection.x; }
		if(newDirection.y < 0) { newDirection.y = arraySize.y + newDirection.y; }
		if(newDirection.z < 0) { newDirection.z = arraySize.z + newDirection.z; }

		return newDirection;
	}

	public static VectorI3 Wrap3DIndex(this VectorI3 positionIndex, VectorI3 direction, VectorI3 arraySize) {
		VectorI3 newDirection = new VectorI3(
			((positionIndex.x + direction.x) % (arraySize.x)),
			((positionIndex.y + direction.y) % (arraySize.y)),
			((positionIndex.z + direction.z) % (arraySize.z))
			);

		if(newDirection.x < 0) { newDirection.x = arraySize.x + newDirection.x; }
		if(newDirection.y < 0) { newDirection.y = arraySize.y + newDirection.y; }
		if(newDirection.z < 0) { newDirection.z = arraySize.z + newDirection.z; }

		return newDirection;
	}

	public static int WrapIndexMod(this int index, int endIndex, int maxSize) {
		return (endIndex + index) % maxSize;
	}


	public static int FlatIndex(this VectorI3 index, VectorI3 size) {
		return index.x + index.y * size.x + index.z * size.x * size.y;
	}

	public static VectorI3 FlatTo3DIndex(this int index, VectorI3 size) {
		return new VectorI3(
			index % size.x,
			index / size.x % size.y,
			index / (size.x * size.y) % size.z
		);
	}

	public static readonly int[] opposite = new int[]{
		1, 0,
		3, 2,
		5, 4,

		7, 6,
		9, 8,
		11, 10,
		13, 12,

		15, 14,
		17, 16,
		19, 18,
		21, 20,
		23, 22,
		25, 24
	};

	public static readonly VectorI3[] direction = new VectorI3[]{
		VectorI3.left,
		VectorI3.right,
		VectorI3.up,
		VectorI3.down,
		VectorI3.forward,
		VectorI3.back,
		
		//Corner
		new VectorI3(-1,1,1),
		new VectorI3(1,-1,-1),

		new VectorI3(-1,-1,1),
		new VectorI3(1,1,-1),

		new VectorI3(-1,1,-1),
		new VectorI3(1,-1,1),

		new VectorI3(-1,-1,-1),
		new VectorI3(1,1,1),

		//Edge Corner
		new VectorI3(-1,0,1), //Left Front
		new VectorI3(1,0,-1), //Right Back
		new VectorI3(-1,0,-1), //Left Back
		new VectorI3(1,0,1), //Right Front

		new VectorI3(-1,1,0), //Left Top
		new VectorI3(1,-1,0), //Right Bottom
		new VectorI3(-1,-1,0), //Left Bottom
		new VectorI3(1,1,0), //Right Top

		new VectorI3(0,1,1), //Top Front
		new VectorI3(0,-1,-1), //Bottom Back
		new VectorI3(0,1,-1), //Top Back
		new VectorI3(0,-1,1), //Bottom Front
	};

	public static VectorI3 Direction(this Direction direction) {
		return MathHelper.direction[(int)direction];
	}

	public static Direction Direction(int direction) {
		return (Direction)Enum.Parse(typeof(Direction), direction.ToString());
	}

	public static VectorI3 IntToDirection(this int index) {
		return direction[index];
	}

	public static int DirectionToIndex(this Vector3 dir) {
		for(int i = 0; i < direction.Length; i++) {
			if(direction[i] == dir) {
				return i;
			}
		}

		return 0;
	}

	public static Vector3 Vector3Clamp(this Vector3 value, Vector3 min, Vector3 max) {
		value.x = value.x > max.x ? value.x = max.x : (value.x < min.x ? value.x = min.x : value.x);
		value.y = value.y > max.y ? value.y = max.y : (value.y < min.y ? value.y = min.y : value.y);
		value.z = value.z > max.z ? value.z = max.z : (value.z < min.z ? value.z = min.z : value.z);

		return value;
	}

	public static int VectorSize(this Vector3 vector) {
		return Mathf.RoundToInt(vector.x * vector.y * vector.z);
	}

	public static Vector3[] OffsetVector3(int count, Vector3[] vectors, Vector3 offset) {
		Vector3[] offsets = new Vector3[count];
		(new List<Vector3>(vectors)).CopyTo(0, offsets, 0, count);

		for(int i = 0; i < count; i++) {
			offsets[i] += offset;
		}

		return offsets;
	}
	public static Vector3[] OffsetVector3(int count, Vector3[] vectors, Vector3[] offset) {
		Vector3[] offsets = new Vector3[count];
		(new List<Vector3>(vectors)).CopyTo(0, offsets, 0, count);

		for(int i = 0; i < offset.Length; i++) {
			offsets[i] += offset[i];
		}

		return offsets;
	}
	public static Vector3[] OffsetVector3(int count, Vector3[] vectors, Vector3[] offset, Vector3 offset2) {
		Vector3[] offsets = new Vector3[count];
		(new List<Vector3>(vectors)).CopyTo(0, offsets, 0, count);

		for(int i = 0; i < count; i++) {
			offsets[i] += offset2 + offset[i];
		}

		return offsets;
	}

	public static Vector2[] OffsetVector2(int count, Vector2[] vectors, Vector2 offset) {
		Vector2[] offsets = new Vector2[count];
		(new List<Vector2>(vectors)).CopyTo(0, offsets, 0, count);

		for(int i = 0; i < count; i++) {
			offsets[i] += offset;
		}

		return offsets;
	}

	public static int[] OffsetInt(int count, int[] ints, int offset) {
		int[] offsets = new int[count];
		(new List<int>(ints)).CopyTo(0, offsets, 0, count);

		for(int i = 0; i < count; i++) {
			offsets[i] += offset;
		}

		return offsets;
	}

	public static Vector3 ParseVector3(string sourceString) {
		string outString;
		Vector3 outVector3;
		string[] splitString = new string[3];

		// Trim extranious parenthesis
		outString = sourceString.Substring(1, sourceString.Length - 2);

		// Split delimted values into an array
		splitString = outString.Split(","[0]);

		// Build new Vector3 from array elements
		outVector3.x = float.Parse(splitString[0]);
		outVector3.y = float.Parse(splitString[1]);
		outVector3.z = float.Parse(splitString[2]);

		return outVector3;
	}
}

public enum Direction {
	Left = 0,
	Right = 1,
	Top = 2, Up = 2,
	Bottom = 3, Down = 3,
	Forward = 4, Front = 4,
	Backward = 5, Back = 5,

	LeftTopFront = 6,
	RightBottomBack = 7,
	LeftBottomFront = 8,
	RightTopBack = 9,
	LeftTopBack = 10,
	RightBottomFront = 11,
	LeftBottomBack = 12,
	RightTopFront = 13,

	LeftFront = 14,
	RightBack = 15,
	LeftBack = 16,
	RightFront = 17,
	LeftTop = 18,
	RightBottom = 19,
	LeftBottom = 20,
	RightTop = 21,
	TopFront = 22,
	BottomBack = 23,
	TopBack = 24,
	BottomFront = 25
}

public enum CVIToDirection {
	LeftTopFront = 6,
	RightTopFront = 13,
	LeftTopBack = 10,
	RightTopBack = 9,
	LeftBottomFront = 8,
	RightBottomFront = 11,
	LeftBottomBack = 12,
	RightBottomBack = 7
}

public enum CubeVertexIndex {
	LeftTopFront = 0,
	RightTopFront = 1,
	LeftTopBack = 2,
	RightTopBack = 3,
	LeftBottomFront = 4,
	RightBottomFront = 5,
	LeftBottomBack = 6,
	RightBottomBack = 7
}
