using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.Timeline;

namespace Helper_Scripts.PlayableShader
{
    [TrackClipType(typeof(ShaderControlAsset))]
    [TrackBindingType(typeof(DecalProjector))]
    public class ShaderTrack : TrackAsset
    {
        public string ShaderVarName;
        public override Playable CreateTrackMixer(
            PlayableGraph graph, GameObject go, int inputCount)
        {
            var mixer = ScriptPlayable<ShaderMixer>.Create(graph, inputCount);
            mixer.GetBehaviour().ShaderVarName = ShaderVarName;
            return mixer;
        }
    }
}