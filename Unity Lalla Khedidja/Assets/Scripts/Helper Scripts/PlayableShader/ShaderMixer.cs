using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Rendering.HighDefinition;

namespace Helper_Scripts.PlayableShader
{
    public class ShaderMixer : PlayableBehaviour
    {
        public string ShaderVarName;

        public override void ProcessFrame(
            Playable playable, FrameData info, object playerData)
        {
            DecalProjector projector = playerData as DecalProjector;
            if (projector == null)
                return;
            int inputCount = playable.GetInputCount();
            float finalFloat = 0;
            for (int index = 0; index < inputCount; index++)
            {
                float weight = playable.GetInputWeight(index);
                var inputPlayable = (ScriptPlayable<ShaderPlayable>) playable.GetInput(index);
                ShaderPlayable behavior = inputPlayable.GetBehaviour();
                finalFloat += behavior.FloatVal * weight;
            }

            projector.material.SetFloat(ShaderVarName, finalFloat);
        }
    }
}