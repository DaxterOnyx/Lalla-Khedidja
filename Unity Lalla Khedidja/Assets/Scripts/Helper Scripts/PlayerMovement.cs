using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 12f;
    
    // Update is called once per frame
    void Update()
    {
        float x;
        float z;
        bool jumpPressed = false;


        x = Input.GetAxis("Horizontal");
        z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;
       

        transform.Translate(move * Time.deltaTime * speed);
    }
}
