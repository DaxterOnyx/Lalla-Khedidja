using UnityEngine;

namespace Cinemachine
{
    public class Activator : MonoBehaviour
    {
        private Activable switcher;

        private void Start()
        {
            switcher = transform.parent.GetComponent<Activable>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                switcher.Modify();
            }
        }
    }
}
