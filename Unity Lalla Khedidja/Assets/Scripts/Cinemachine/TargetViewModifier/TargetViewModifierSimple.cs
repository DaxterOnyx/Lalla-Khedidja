using UnityEngine;

namespace Cinemachine
{
    public class TargetViewModifierSimple : TargetViewModifier
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
                Modify();
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Player"))
                Unmodify();
        }

    }
}
