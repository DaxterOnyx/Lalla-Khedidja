using UnityEngine;

namespace Cinemachine
{
    public class TargetViewModifier : Activable
    {
        public Transform Target;
        [Range(0, 100)] 
        public float Weight = 1;
        [Tooltip("Size of the target"), Min(0)]
        public float Size = 1;
        public override void Activate()
        { 
            PlayerCamera.Instance.AddInterestPoint(Target, Weight, Size);
        }
    
        public override void Deactivate()
        {
            PlayerCamera.Instance.RemoveInterestPoint(Target);
        }
    }
}