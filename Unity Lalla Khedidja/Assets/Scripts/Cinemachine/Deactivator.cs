using Cinemachine;
using UnityEngine;

public class Deactivator : MonoBehaviour
{
    private Activable activable;

    private void Start()
    {
        activable = transform.parent.GetComponent<Activable>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            activable.Unmodify();
        }
    }
}
