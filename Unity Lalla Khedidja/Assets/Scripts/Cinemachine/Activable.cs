using UnityEngine;

namespace Cinemachine
{
    public abstract class Activable : MonoBehaviour
    {
        public bool ActivateOnce = false;
        public float MaxTime = -1f;
        public bool StopPlayer = false;
        private bool _isAlreadyActivated = false;
        private bool _isInside = false;
        public abstract void Activate();
        public abstract void Deactivate();

        public void Modify()
        {
            if (!_isAlreadyActivated)
            {
                if (!_isInside)
                {
                    Activate();
                    if (ActivateOnce)
                        _isAlreadyActivated = true;
                    if (MaxTime > 0)
                        Invoke(nameof(Deactivate), MaxTime);
                    if (StopPlayer)
                        PlayerControl.Instance.StopQuiet();
                }
            }

            _isInside = !_isInside;
        }

        public void Unmodify()
        {
            if (_isInside)
                Deactivate();
            _isInside = !_isInside;
        }
    }
}