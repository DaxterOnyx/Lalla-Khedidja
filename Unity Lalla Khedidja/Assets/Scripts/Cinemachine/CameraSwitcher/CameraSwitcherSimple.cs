using System.Collections;
using System.Collections.Generic;
using Cinemachine.CameraSwitcher;
using UnityEngine;

public class CameraSwitcherSimple : CameraSwitcher
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Modify();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
            Unmodify();
    }
}
