using UnityEngine;

namespace Cinemachine.CameraSwitcher
{
    public class CameraSwitcher : Activable
    {
        public CinemachineVirtualCamera Camera;
        
        private void Start()
        {
            Camera.enabled = false;
        }

        public override void Activate()
        {
            PlayerCamera.Instance.DisableMainCamera();
                Camera.enabled = true;
        }

        public override void Deactivate()
        {
            PlayerCamera.Instance.EnableMainCamera();
            Camera.enabled = true;
        }
    }
}
