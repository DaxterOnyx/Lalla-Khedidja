using System;
using FMODUnity;
using UnityEngine;

public class CheckTerrainTexture : MonoBehaviour
{
    public Transform FrontRightFoot;
    public Transform FrontLeftFoot;
    public Transform BackRightFoot;
    public Transform BackLeftFoot;

    [Serializable]
    public class LayerEvent { public LayerMask Layer;[EventRef] public string Event; }

    [SerializeField]
    public LayerEvent[] EventByLayer;

    [EventRef] public string[] TerrainEvents;

    GameObject GetFloor(Vector3 pos)
    {
        GameObject f = null;
        if (Physics.Raycast(pos, Vector3.down, out var hit))
            f = hit.transform.gameObject;

        return f;
    }

    Vector2Int ConvertPosition(Terrain t, Vector3 playerPosition)
    {
        Vector3 terrainPosition = playerPosition - t.transform.position;

        Vector3 mapPosition = new Vector3
        (terrainPosition.x / t.terrainData.size.x, 0,
            terrainPosition.z / t.terrainData.size.z);

        float xCoord = mapPosition.x * t.terrainData.alphamapWidth;
        float zCoord = mapPosition.z * t.terrainData.alphamapHeight;

        Vector2Int pos = new Vector2Int((int)xCoord, (int)zCoord);
        return pos;
    }

    float[] CheckTexture(Terrain t, Vector2Int pos)
    {
        var textureValues = new float[TerrainEvents.Length];
        float[,,] aMap = t.terrainData.GetAlphamaps(pos.x, pos.y, 1, 1);
        if (aMap.GetLength(2) != TerrainEvents.Length)
            Debug.LogError("Terrain Alphamaps does not have how many texture as Texture Event");
        for (int i = 0; i < TerrainEvents.Length; i++)
        {
            textureValues[i] = aMap[0, 0, i];
        }

        return textureValues;
    }

    void PlayFootstep(Vector3 pos)
    {
        var obj = GetFloor(pos);

        if (obj != null)
        {
            string path = "";
            var terrain = obj.GetComponent<Terrain>();
            if (terrain != null)
            {
                //TERRAIN
                var posTerrain = ConvertPosition(terrain, pos);
                var textureValues = CheckTexture(terrain, posTerrain);
                var i = 0;
                for (int j = 1; j < TerrainEvents.Length; j++)
                {
                    if (textureValues[i] < textureValues[j])
                        i = j;
                }
                path = TerrainEvents[i];
            }
            else
            {
                foreach (LayerEvent layerEvent in EventByLayer)
                {
                    if (PlayerControl.IsLayer(layerEvent.Layer, obj.layer))
                    {
                        path = layerEvent.Event;
                    }
                }
            }

            if (path == "")
            {
                Debug.LogWarning("Event");
            }

            //Debug.Log("Foot event : "+path);
            RuntimeManager.PlayOneShot(path);

        }

    }

    public void PlayFootstepFrontRight() => PlayFootstep(FrontRightFoot.position);
    public void PlayFootstepFrontLeft() => PlayFootstep(FrontLeftFoot.position);
    public void PlayFootstepBackRight() => PlayFootstep(BackRightFoot.position);
    public void PlayFootstepBackLeft() => PlayFootstep(BackLeftFoot.position);
}