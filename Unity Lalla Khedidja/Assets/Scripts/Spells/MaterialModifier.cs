using UnityEngine;
using UnityEngine.Rendering.HighDefinition;

[ExecuteInEditMode]
public class MaterialModifier : MonoBehaviour
{
    private Renderer renderer;
    public string ParameterName = "Size";
    public float ParameterValue = 0f;

    private float lastValue;

    void Start()
    {
        lastValue = ParameterValue - 1f;
        renderer = GetComponent<MeshRenderer>();
    }

    void LateUpdate()
    {
        if (Mathf.Abs(lastValue - ParameterValue) > float.Epsilon)
        {
            if (renderer == null) renderer = GetComponent<MeshRenderer>();
#if UNITY_EDITOR
            renderer.sharedMaterial.SetFloat(ParameterName, ParameterValue);
#else
            renderer.material.SetFloat(ParameterName, ParameterValue);
#endif
            lastValue = ParameterValue;
        }
    }
}