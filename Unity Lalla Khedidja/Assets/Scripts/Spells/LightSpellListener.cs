using FMODUnity;
using Unity.Mathematics;
using UnityEditor;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.Timeline;

namespace Spells
{
    public class LightSpellListener : MonoBehaviour
    {
        private Volume volume;

        private bool _isSee = false;
        private bool _wasSee = false;

        private bool _isSpellActivate = false;

        [EventRef] public string EnterEvent;
        [EventRef] public string ExitEvent;
        [EventRef] public string BlindEvent;
        [EventRef] public string UnblindEvent;

        private PlayableDirector playable;

        // Start is called before the first frame update
        void Awake()
        {
            

            playable = GetComponent<PlayableDirector>();
            volume = GetComponentInChildren<Volume>();
            foreach (TrackAsset track in ((TimelineAsset) playable.playableAsset).GetOutputTracks())
                if (playable.GetGenericBinding(track) == null)
                    playable.SetGenericBinding(track, PlayerControl.Instance);
        }

        private void Update()
        {
            var deltaTime = Time.deltaTime;

            if (_wasSee)
            {
                double time;
                if (_isSee && !_isSpellActivate)
                {
                    time = playable.time + deltaTime;
                }
                else
                {
                    time = playable.time - deltaTime;
                    if (time <= 0)
                        _wasSee = false;
                }

                playable.time = math.clamp(time, 0, playable.duration);
                if (playable.time >= playable.duration)
                    RuntimeManager.PlayOneShot(BlindEvent);

                playable.Evaluate();

            }
        }

        protected void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag("Player"))
                return;
            PlayerControl.Instance.LaunchLightSpell.AddListener(ActiveSpell);
            PlayerControl.Instance.EndLightSpell.AddListener(EndSpell);
            Debug.Log("LightSpell Possible");

            RuntimeManager.PlayOneShot(EnterEvent);
            //Calcul time inside shadow area
            _isSee = true;
            _wasSee = true;

            //TODO ADD SOUND OF SAD DONKEY IN DARKNESS
        }

        protected void OnTriggerExit(Collider other)
        {
            if (!other.CompareTag("Player"))
                return;

            PlayerControl.Instance.LaunchLightSpell.RemoveListener(ActiveSpell);

            RuntimeManager.PlayOneShot(ExitEvent);
            RuntimeManager.PlayOneShot(UnblindEvent);
            Debug.Log("LightSpell not Possible");

            _isSee = false;
        }

        protected void ActiveSpell()
        {
            RuntimeManager.PlayOneShot(UnblindEvent);
            _isSpellActivate = true;
        }

        protected void EndSpell()
        {
            _isSpellActivate = false;
        }
    }
}