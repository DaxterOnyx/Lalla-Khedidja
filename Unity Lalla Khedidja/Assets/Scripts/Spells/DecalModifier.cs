using System;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;

[ExecuteInEditMode]
public class DecalModifier : MonoBehaviour
{
    private DecalProjector projector;
    public string ParameterName = "Size";
    public float ParameterValue = 0f;

    private float lastValue;

    void Start()
    {
        lastValue = ParameterValue - 1f;
        projector = GetComponent<DecalProjector>();
    }

    void Update()
    {
        if (Math.Abs(lastValue - ParameterValue) > float.Epsilon)
        {
            projector.material.SetFloat(ParameterName, ParameterValue);
            lastValue = ParameterValue;
        }
    }
}