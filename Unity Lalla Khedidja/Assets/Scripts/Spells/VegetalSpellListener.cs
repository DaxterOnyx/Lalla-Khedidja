using UnityEngine;

namespace Spells
{
    public class VegetalSpellListener : MonoBehaviour
    {
        public GameObject Vegetation;

        public GameObject Bush1;
        public GameObject Bush2;
      
        private void Awake()
        {
            Vegetation.SetActive(false);
        }

        private void OnTriggerEnter(Collider other)
        {
            
            if (other.CompareTag("Player"))
            {
                
                PlayerControl.Instance.VegetalSpell.AddListener(ActiveSpell);
                Debug.Log("VegetalSpell Possible");
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                PlayerControl.Instance.VegetalSpell.RemoveListener(ActiveSpell);
                Debug.Log("VegetalSpell not Possible");
            }
        }

        private void ActiveSpell()
        {
            Bush1.SetActive(false);
            Bush2.SetActive(false);
            Vegetation.SetActive(true);
            PlayerControl.Instance.VegetalSpell.RemoveListener(ActiveSpell);
        }
    }
}
