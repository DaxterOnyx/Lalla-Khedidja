using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWS;

public class waypointFollower : MonoBehaviour {

    // le script fonctionne avec le plugin simple waypoint disponible sur l'asset store
    // nécessite d'avoir une liste de point définis grâce au script Waypoint manager ou d'entrer les coordonnées des points à la main

    // Permet à la cible de suivre le tag player au plus près tout en restant sur un axe de points définis en amont (utile pour
    // créer une seule instance d'un event sonore comme une rivière).

    public PathManager manager;
    public Vector3[] waypoints;

    private Transform player;

    void Awake()
    {
        if (manager == null)
            manager = transform.parent.GetComponent<PathManager>();
        
        // fonctions spécifiques
        waypoints = new Vector3[manager.waypoints.Length];
        for (var i = 0; i < manager.waypoints.Length; i++)
        {
            waypoints[i] = manager.waypoints[i].position;
        }

        player = GameObject.FindGameObjectWithTag("Player").transform;

    }

    void Update()
    {
        // trie les waypoints par distance

        System.Array.Sort<Vector3>(waypoints, delegate (Vector3 way1, Vector3 way2) {
            return Vector3.Distance(way1, player.position).CompareTo(Vector3.Distance(way2, player.position));
        });

        // trouve les waypoints les plus proche et crée un chemin entre les deux

        transform.position = ClosestPointOnLine(waypoints[0], waypoints[1], player.position);
    }

    Vector3 ClosestPointOnLine(Vector3 vA, Vector3 vB, Vector3 vPoint)
    {
        var vVector1 = vPoint - vA;
        var vVector2 = (vB - vA).normalized;

        var d = Vector3.Distance(vA, vB);
        var t = Vector3.Dot(vVector2, vVector1);

        if (t <= 0)
            return vA;

        if (t >= d)
            return vB;

        var vVector3 = vVector2 * t;

        var vClosestPoint = vA + vVector3;

        return vClosestPoint;
    }
}
