using System;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent( /*typeof(Rigidbody),*/ typeof(SerialController))]
public class PlayerControl : MonoBehaviour
{
    [Header("Movement")] public float MaxRotationSpeed = 30;
    public AnimationCurve RotationSpeedInterpolation = AnimationCurve.EaseInOut(0, 0, 1, 1);
    public float MaxWalkSpeed = 4;
    public AnimationCurve WalkInterpolation = AnimationCurve.EaseInOut(0, 0, 1, 1);

    [Header("Timeline Parameter"), SerializeField /*, Range(0, 1)*/]
    public float MaxRatioSpeedControl = 1;

    [Header("References")] public Transform RaycastStickToFloor;
    public Transform CheckIncline;

    [Header("Layer Interaction")] public LayerMask LayerTerrain;
    public LayerMask LayerToClimb;
    public LayerMask LayerToEscape;

    [Header("Climb Parameter")] public float MaxAngleClimb = 30;
    public float MaxDistClimb = 1;

    [Header("Wall Evitate")] [Min(0f)] public float DistToStartEvitate = 10f;
    public AnimationCurve SpeedEvitateProgression = AnimationCurve.Linear(0, 0, 1, 1);
    private GameObject _currentFloor;
    private Rigidbody _rb;

    internal float Rotation;
    private float _speed;

    [Header("Sound Control")] 
    public float IncreaseSpeedCallYa = 0.5f;
    public float DecreaseSpeedCallHo = 0.25f;
    
    internal float Speed
    {
        get
        {
            if (_isStopped)
                return 0;
            if (_speed > MaxRatioSpeedControl)
                _speed = MaxRatioSpeedControl;
            return _speed;
        }
        set
        {
            if (!_isStopped 
                || (_isStopped && Math.Abs(value - _speed) > 0.1f))
            {
                _isStopped = false;
                if (value > MaxRatioSpeedControl)
                    value = MaxRatioSpeedControl;
                if (value - _speed >= IncreaseSpeedCallYa)
                    BigAcceleration.Invoke();
                else if (_speed - value >= DecreaseSpeedCallHo)
                    BigDeceleration.Invoke();

                _speed = value;
            }
        }
    }

    internal UnityEvent VegetalSpell = new UnityEvent();
    internal UnityEvent LaunchLightSpell = new UnityEvent();
    internal UnityEvent EndLightSpell = new UnityEvent();
    internal UnityEvent DonkeyStop = new UnityEvent();
    internal UnityEvent BigAcceleration = new UnityEvent();
    internal UnityEvent BigDeceleration = new UnityEvent();

    private bool _isStopped;

    public static PlayerControl Instance { private set; get; } 
    
    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
        Instance = this;
    }

    private void Update()
    {
        Rotate();
    }

    private void FixedUpdate()
    {
        //Climb management
        ManageFrontSpace();

        //CHECK COLLISION IN FRONT
        ManageFrontCollision();

        //Speed management
        MoveForward();

        ManageFloorContact();
    }

    private void Rotate()
    {
        //Initial rotation
        Vector3 angularVelocity = _rb.angularVelocity;
        //Interpolate speed with Curve
        float rotationSpeed =
            Mathf.Sign(Rotation) * RotationSpeedInterpolation.Evaluate(Mathf.Abs(Rotation));
        //Rotate player
        angularVelocity = new Vector3(angularVelocity.x, rotationSpeed * Time.fixedDeltaTime * MaxRotationSpeed,
            angularVelocity.z);
        transform.Rotate(angularVelocity);
    }

    private void MoveForward()
    {
        if (Speed != 0 && !_isStopped)
        {
            if (Speed > MaxRatioSpeedControl)
                Speed = MaxRatioSpeedControl;
            float walkSpeed = MaxWalkSpeed * WalkInterpolation.Evaluate(Speed);
            transform.Translate(
                new Vector3(0, 0, walkSpeed * Time.fixedDeltaTime), Space.Self);
        }
        else
        {
            _rb.velocity =
                new Vector3(0, _rb.velocity.y, 0);
        }
    }

    private void ManageFrontSpace()
    {
        Ray rayFloor = new Ray(CheckIncline.position, Vector3.down);
        RaycastHit[] hits = Physics.RaycastAll(rayFloor);
        RaycastHit? hitFloor = null;
        if (hits.Length > 0)
        {
            RaycastHit? water = null;
            RaycastHit? terrain = null;
            RaycastHit? climb = null;
            int objectLayer;
            foreach (var hit in hits)
            {
                objectLayer = hit.transform.gameObject.layer;
                if (IsLayer(LayerToClimb, objectLayer))
                {
                    //LAYER OF BRIDGE
                    if (!climb.HasValue || hit.distance < climb.Value.distance)
                        climb = hit;
                }
                else if (IsLayer(LayerToEscape, objectLayer) || objectLayer == LayerToEscape)
                {
                    //LAYER OF WATER
                    if (!water.HasValue || hit.distance < water.Value.distance)
                        water = hit;
                }
                else if (IsLayer(LayerTerrain, objectLayer))
                {
                    if (!terrain.HasValue || hit.distance < terrain.Value.distance)
                        terrain = hit;
                }
            }

            if (climb.HasValue && (!water.HasValue || climb.Value.distance < water.Value.distance) &&
                (!terrain.HasValue || climb.Value.distance < terrain.Value.distance))
            {
                hitFloor = climb.Value;
                //LAYER OF BRIDGE
            }
            else if (water.HasValue && (!terrain.HasValue || water.Value.distance < terrain.Value.distance))
            {
                hitFloor = water.Value;
                //LAYER OF WATER
                Stop();
            }
            else if (terrain.HasValue)
            {
                hitFloor = terrain.Value;
                //NORMAL LAYER TO WALKING
                float angle = Vector3.Angle(hitFloor.Value.normal, Vector3.up);
                //Debug.Log(angle);
                if (angle >= MaxAngleClimb || hitFloor.Value.distance > MaxDistClimb)
                    Stop();
                Debug.DrawLine(CheckIncline.position, hitFloor.Value.point,
                    hitFloor.Value.distance > MaxDistClimb ? Color.red :
                    angle >= 90 - MaxAngleClimb ? Color.yellow : Color.blue);
            }
            else
            {
                //EMPTY SPACE IN FRONT
                Stop();
            }

            if (hitFloor.HasValue)
            {
                //TODO SEARCH BETTER STRAT
                //var dir = Quaternion.LookRotation(Vector3.forward, hitFloor.Value.normal).eulerAngles;
                //CheckIncline.localRotation = Quaternion.Euler(-1*Mathf.Abs(dir.z),0,0);
            }
        }
        else
        {
//EMPTY SPACE IN FRONT
            Stop();
        }
    }

    private void ManageFrontCollision()
    {
        Ray rayFront = new Ray(CheckIncline.position, CheckIncline.forward);
        RaycastHit rayHit;
        if (Physics.Raycast(rayFront, out rayHit, DistToStartEvitate))
        {
            //if(rayHit)
            Debug.DrawLine(CheckIncline.position, rayHit.point);
            if (IsLayer(LayerToClimb, rayHit.transform.gameObject.layer))
                return;
            if (IsLayer(LayerTerrain, rayHit.transform.gameObject.layer))
                return;
            float maxRatio;
            maxRatio = 1f - SpeedEvitateProgression.Evaluate(1f - (rayHit.distance / DistToStartEvitate));
            Speed = Mathf.Min(maxRatio, Speed);
        }
    }

    internal static bool IsLayer(LayerMask layerMask, int layer)
    {
        return (layerMask & (1 << layer)) != 0;
    }

    private void ManageFloorContact()
    {
        Ray rayFloor = new Ray(RaycastStickToFloor.position, Vector3.down);

        var layer = LayerTerrain | LayerToClimb;
        if (Physics.Raycast(rayFloor, out var raycastFloorHit, Mathf.Infinity, layer))
        {
            Debug.DrawLine(RaycastStickToFloor.position, raycastFloorHit.point, Color.red);
            //TODO Inverse kinematic

            GameObject floor = raycastFloorHit.collider.gameObject;

            //Teleport on the floor
            if (_currentFloor != null)
            {
                if (_currentFloor != floor && raycastFloorHit.distance > MaxDistClimb)
                    return;
            }
            else
            {
                _currentFloor = floor;
            }

            transform.position = raycastFloorHit.point - 0.05f * Vector3.down;
        }
    }

    internal void TurnAtRight(float ratio) => Rotation = ratio;
    internal void TurnAtLeft(float ratio) => Rotation = -ratio;

    internal void TurnRight(float ratio)
    {
        Rotation = Mathf.Min(1, Rotation + ratio);
    }

    internal void TurnLeft(float ratio)
    {
        Rotation = Mathf.Max(-1, Rotation - ratio);
    }

    internal void Accelerate(float ratio)
    {
        AccelerateAt(Mathf.Min(1f, Speed + ratio));
    }

    internal void Decelerate(float ratio)
    {
        AccelerateAt(Mathf.Max(0, Speed - ratio));
    }

    public void StopQuiet()
    {
        _isStopped = true;
        Rotation = 0;
    }

    public void Stop()
    {
        _speed = 0;
        DonkeyStop.Invoke();
    }

    public void AccelerateAt(float ratio)
    {
        Speed = ratio;
    }
}