using UnityEngine;

namespace Player
{
    public class PlayerTP : MonoBehaviour
    {
        public Transform[] Locations;
    
        // Update is called once per frame
        void Update()
        {
            //TODO REMOVE
            if (Input.GetKeyDown(KeyCode.Alpha0))
                Tp(Locations[0]);
            if (Input.GetKeyDown(KeyCode.Alpha1))
                Tp(Locations[1]);
            if (Input.GetKeyDown(KeyCode.Alpha2))
                Tp(Locations[2]);
            if (Input.GetKeyDown(KeyCode.Alpha3))
                Tp(Locations[3]);
            if (Input.GetKeyDown(KeyCode.Alpha4))
                Tp(Locations[4]);
            if (Input.GetKeyDown(KeyCode.Alpha5))
                Tp(Locations[5]);
            if (Input.GetKeyDown(KeyCode.Alpha6))
                Tp(Locations[6]);
            if (Input.GetKeyDown(KeyCode.Alpha7))
                Tp(Locations[7]);
            if (Input.GetKeyDown(KeyCode.Alpha8))
                Tp(Locations[8]);
            if (Input.GetKeyDown(KeyCode.Alpha9))
                Tp(Locations[9]);
        }

        private void Tp(Transform target)
        {
            transform.position = target.position;
            transform.rotation = target.rotation;
        }
    }
}
