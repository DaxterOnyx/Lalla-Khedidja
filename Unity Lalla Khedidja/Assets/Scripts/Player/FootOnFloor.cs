using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootOnFloor : MonoBehaviour
{
    public Vector3 offset = Vector3.up;
    public Transform FootRef;
    public LayerMask FloorLayer;
    
    private Vector3 startPosDiff;
    private Quaternion startRotDiff;
    
    private Vector3 startPos;
    private Quaternion startRot;
    
    private void Start()
    {
        startPos = transform.localPosition;
        startRot = transform.localRotation;

        startPosDiff = startPos - FootRef.position;
        startRotDiff = FootRef.rotation;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        var ray = new Ray(FootRef.position + offset, Vector3.down);

        if (Physics.Raycast(ray, out var hit, 2f, FloorLayer))
        {
            Debug.DrawLine(FootRef.position + offset, FootRef.position + Vector3.down);
            transform.position = hit.point;
            transform.rotation = Quaternion.FromToRotation(Vector3.up, hit.normal);
        }
        else
        {
            transform.localPosition = startPos;
            transform.localRotation = startRot;
        }
            
    }
}