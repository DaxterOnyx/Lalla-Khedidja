using DG.Tweening;
using UnityEngine;

public class PlayerSpell : MonoBehaviour
{
    public PlayerControl player;
    [Header("LightSpell")]
    public GameObject LightSpellPrefab;
    public Transform LightSpellRef;
    public Vector3 Destination = Vector3.zero;
    public float TimeSpellAlive = 8f;
    private float timeAlive;
    private bool lightSpellAlive = false;
    [Header("VegetalSpell")]
    public GameObject VegetalSpellPrefab;
    public Transform VegetalSpellRef;
    
    private GameObject currentSpell;

    public void SpawnLightSpell()
    {
        SpawnSpell(LightSpellPrefab, LightSpellRef);
        timeAlive = TimeSpellAlive;
        lightSpellAlive = true;
        currentSpell.transform.DOLocalMove(Destination, 2f);
    }

    private void Update()
    {
        if (lightSpellAlive)
        {
            timeAlive -= Time.deltaTime;
            if (timeAlive <= 0)
            {
                EndLightSpell();
            }
        }
    }

    public void SpawnVegetalSpell()
    {
        SpawnSpell(VegetalSpellPrefab, VegetalSpellRef);
    }

    private void SpawnSpell(GameObject spellPrefab, Transform parent)
    {
        if (currentSpell != null)
            Destroy(currentSpell);
        currentSpell = Instantiate(spellPrefab, parent.position,parent.rotation,player != null ? player.transform : null);
    }

    public void EndLightSpell()
    {
        lightSpellAlive = false;
        Destroy(currentSpell);
        player?.EndLightSpell.Invoke();
    }

    public void EndVegetalSpell()
    {
        Destroy(currentSpell);
    }
}