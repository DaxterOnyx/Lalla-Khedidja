using System;
using FMODUnity;
using UnityEngine;

public class PlayerSound : MonoBehaviour
{
    private int inHerb = 0;

    [EventRef] public string EnterHerb;
    [EventRef] public string ExitHerb;

    private void OnCollisionEnter(Collision other)
    {
        if (!other.collider.CompareTag("Herb"))
            return;
        if(inHerb == 0)
            RuntimeManager.PlayOneShot(EnterHerb);
        inHerb++;
    }
    
    private void OnCollisionExit(Collision other)
    {
        if (other.transform.CompareTag("Herb"))
            inHerb--;
        
        if(inHerb == 0)
            RuntimeManager.PlayOneShot(ExitHerb);

    }
}
