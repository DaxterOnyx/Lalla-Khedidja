using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Cinemachine;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    private static PlayerCamera _instance;

    public static PlayerCamera Instance => _instance;

    public CinemachineTargetGroup TargetGroup;
    public CinemachineBrain Brain;

    public CinemachineVirtualCamera MainCam;

    private void Start()
    {
        _instance = this;
    }

    public void AddInterestPoint(Transform t, float weight, float size)
    {
        TargetGroup.AddMember(t, weight, size);
    }

    public void RemoveInterestPoint(Transform t)
    {
        TargetGroup.RemoveMember(t);
    }

    public void ChangeWeight(Transform t, float weight, float size)
    {
        CinemachineTargetGroup.Target target = TargetGroup.m_Targets[TargetGroup.FindMember(t)];
        target.weight = weight;
        target.radius = size;
    }

    public void DisableMainCamera()
    {
        MainCam.enabled = false;
    }

    public void EnableMainCamera()
    {
        MainCam.enabled = true;
    }
}