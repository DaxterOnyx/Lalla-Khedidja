using UnityEngine;

public class AnimationTest : MonoBehaviour
{
    [Range(0f, 1f)] public float Speed = 0;
    [Range(-1f, 1f)] public float Rotation = 0;
    public bool LightSpell = false;
    public bool VegetalSpell = false;

    private Animator[] animators;

    private float time;

    // Start is called before the first frame update
    void Start()
    {
        time = 0;
        animators = GetComponentsInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        foreach (Animator animator in animators)
        {
            animator.SetFloat("Time", time);

            animator.SetFloat("Speed", Speed);
            animator.SetFloat("Rotation", Rotation);
            animator.SetLayerWeight(2, Mathf.Abs(Rotation));

            if (LightSpell)
                animator.SetTrigger("LightSpell");

            if (VegetalSpell)
                animator.SetTrigger("VegetalSpell");
        }
    }
}