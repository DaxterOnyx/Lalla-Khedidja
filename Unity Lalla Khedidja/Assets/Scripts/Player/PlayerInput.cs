using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInput : MonoBehaviour
{
    private PlayerControl _playerControl;

    [Header("Arduino Parameter")]
    [Range(0, 1)]
    public float RotationDeadzone = 0.05f;

    [Range(0, 1)] public float AccelerationDeadzone = 0.05f;

    [Range(0, 1)] public float AccelerationAverage = 0.8f;
    [Range(0, 1)] public float SpellGap = 0.1f;
    public float SpellLatence = 3f;
    private float timelastSpell = -1f;

    [Header("Computer Inputs")] public InputAction InputMove;
    public InputAction InputAccelerate;
    public InputAction InputDecelerate;
    public InputAction InputLeft;
    public InputAction InputRight;
    public InputAction InputLeftAnalog;
    public InputAction InputRightAnalog;
    public InputAction InputVegetalSpell;
    public InputAction InputLightSpell;


    //GAMEPAD VARIABLES
    private bool accelerate = false;
    private float acceleration = 0f;
    private bool decelerate = false;
    private float deceleration = 0f;
    private bool turningRight = false;
    private bool pullRight = false;
    private float turnRight = 0f;
    private bool pullLeft = false;
    private bool turningLeft = false;
    private float turnLeft = 0f;

    // Start is called before the first frame update
    void Start()
    {
        _playerControl = GetComponent<PlayerControl>();

        //INPUT

        #region Setting Input

        InputAccelerate.started += context =>
        {
            accelerate = true;
            acceleration = 0f;
        };
        InputAccelerate.canceled += context =>
        {
            accelerate = false;
            acceleration = 0f;
        };
        InputDecelerate.started += context =>
        {
            decelerate = true;
            deceleration = 0f;
        };
        InputDecelerate.canceled += context =>
        {
            decelerate = false;
            deceleration = 0f;
        };
        InputLeft.started += context =>
        {
            pullLeft = true;
            turningLeft = true;
            turnLeft = 0f;
        };
        InputLeft.canceled += context =>
        {
            pullLeft = false;
            turningLeft = false;
            turnLeft = 0f;
        };
        InputRight.started += context =>
        {
            pullRight = true;
            turningRight = true;
            turnRight = 0f;
        };
        InputRight.canceled += context =>
        {
            pullRight = false;
            turningRight = false;
            turnRight = 0f;
        };

        InputMove.performed += InputJoystick;
        InputLeftAnalog.performed += LeftAnalog;
        InputLeftAnalog.started += context => pullLeft = true;
        InputLeftAnalog.canceled += context => pullLeft = false;
        InputRightAnalog.performed += RightAnalog;
        InputRightAnalog.started += context => pullRight = true;
        InputRightAnalog.canceled += context => pullRight = false;
        InputVegetalSpell.performed += context => _playerControl.VegetalSpell.Invoke();
        InputLightSpell.performed += context => _playerControl.LaunchLightSpell.Invoke();

        #endregion

        InputAccelerate.Enable();
        InputDecelerate.Enable();
        InputLeft.Enable();
        InputRight.Enable();
        InputMove.Enable();
        InputVegetalSpell.Enable();
        InputLightSpell.Enable();
    }

    #region Gamepad

    private void Update()
    {
        if (accelerate)
        {
            acceleration = Mathf.Min(1, acceleration + Time.deltaTime);
            _playerControl.Accelerate(acceleration);
        }

        if (decelerate)
        {
            deceleration = Mathf.Min(1, deceleration + Time.deltaTime);
            _playerControl.Decelerate(deceleration);
        }

        if (pullLeft && pullRight)
            _playerControl.Stop();
        if (turningLeft)
        {
            turnLeft = Mathf.Min(1, turnLeft + Time.deltaTime);
            _playerControl.TurnLeft(turnLeft);
        }

        if (turningRight)
        {
            turnRight = Mathf.Min(1, turnRight + Time.deltaTime);
            _playerControl.TurnRight(turnRight);
        }

        if (timelastSpell > 0)
            timelastSpell -= Time.deltaTime;
    }

    private void InputJoystick(InputAction.CallbackContext obj)
    {
        float deadZone = 0.2f;

        var t = obj.action.ReadValue<Vector2>();
        // Y
        if (t.y > deadZone)
            _playerControl.Accelerate(t.y);
        else if (t.y < -1 * deadZone)
            _playerControl.Decelerate(-1 * t.y);

        // X
        // if (t.x > deadZone)
        //     donkeyControl.TurnRight(t.x);
        // else
        //     donkeyControl.StopTurnRight();
        //
        // if (t.x < -1 * deadZone)
        //     donkeyControl.TurnLeft(-1 * t.x);
        // else
        //     donkeyControl.StopTurnLeft();
    }

    private void RightAnalog(InputAction.CallbackContext ctx)
    {
        var t = ctx.action.ReadValue<float>();
        _playerControl.TurnAtRight(t);
    }

    private void LeftAnalog(InputAction.CallbackContext ctx)
    {
        var t = ctx.action.ReadValue<float>();
        _playerControl.TurnAtLeft(t);
    }

    #endregion

    #region ARDUINO

    // Invoked when a line of data is received from the serial device.
    void OnMessageArrived(string msg)
    {
        Debug.Log("Arrived: " + msg);

        //String output = String(inputVegetal) + " " + String(inputLight) + " " + String(valLeft) + " " + String(valRight) + " " + String(valAcc);

        string[] strings = msg.Split(' ');

        if (strings[0] == "Initialisation")
        {
            Debug.Log("Initialisation");
            //TODO RESET PARAMETERS
        }
        else
        {
            bool vegetalSpell = ((float)int.Parse(strings[0])) / 1000f >= SpellGap;
            bool lightSpell = ((float)int.Parse(strings[1])) / 1000f >= SpellGap;
            float leftRatio = 1f - (int.Parse(strings[2]) / 1000f);
            float rightRatio = 1f - (int.Parse(strings[3]) / 1000f);
            float accRatio = int.Parse(strings[4]) / 1000f;

            if (vegetalSpell && timelastSpell <= 0)
            {
                timelastSpell = SpellLatence;
                _playerControl.VegetalSpell.Invoke();
            }

            if (lightSpell && timelastSpell <= 0)
            {
                timelastSpell = SpellLatence;
                _playerControl.LaunchLightSpell.Invoke();
            }


            if (leftRatio > RotationDeadzone)
                _playerControl.TurnAtLeft(leftRatio);
            else if (rightRatio > RotationDeadzone)
                _playerControl.TurnAtRight(rightRatio);
            else
                _playerControl.TurnAtLeft(0);

            if (accRatio > AccelerationAverage + AccelerationDeadzone)
            {
                var ratio = (accRatio - (AccelerationAverage + AccelerationDeadzone)) /
                            (1f - (AccelerationAverage + AccelerationDeadzone));
                _playerControl.AccelerateAt(ratio);
            }
            else if (accRatio < AccelerationAverage - AccelerationDeadzone)
            {
                var ratio = ((AccelerationAverage + AccelerationDeadzone) - accRatio) /
                            (1f - (AccelerationAverage + AccelerationDeadzone));
                _playerControl.AccelerateAt(0);
            }
        }
    }

    // Invoked when a connect/disconnect event occurs. The parameter 'success'
    // will be 'true' upon connection, and 'false' upon disconnection or
    // failure to connect.
    void OnConnectionEvent(bool success)
    {
#if UNITY_EDITOR
        Debug.LogWarning(success ? "Device connected" : "Device disconnected");
#endif
    }

    #endregion
}