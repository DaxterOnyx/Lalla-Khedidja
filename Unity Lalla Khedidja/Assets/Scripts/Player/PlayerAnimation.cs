using FMODUnity;
using UnityEngine;
using UnityEngine.Animations.Rigging;

[RequireComponent(typeof(PlayerControl))]
public class PlayerAnimation : MonoBehaviour
{
    private PlayerControl playerControl;

    [Header("Animation")] public Animator DonkeyAnimator;
    public Animator ThiziriAnimator;

    [Header("Rig")] public RigBuilder RigBuilder;

    [Header("Camera")] public Transform LookDirection;
    public float LeftMaxAngle = 60;
    public float RightMaxAngle = 60;
    public AnimationCurve RotationCamera = AnimationCurve.EaseInOut(0, 0, 1, 1);

    [Header("Sounds")] [EventRef] public string VegetalSpellEvent = "";
    [EventRef] public string LightSpellEvent = "";
    [EventRef] public string DonkeyStopEvent = "";
    [EventRef] public string AccelerateEvent = "";
    [EventRef] public string DecelerateEvent = "";

    private float time;

    private void Start()
    {
        playerControl = GetComponent<PlayerControl>();
        playerControl.LaunchLightSpell.AddListener(LightSpell);
        playerControl.VegetalSpell.AddListener(VegetalSpell);
        playerControl.DonkeyStop.AddListener(DonkeyStop);
        playerControl.BigAcceleration.AddListener(Acceleration);
        playerControl.BigDeceleration.AddListener(Deceleration);

        if (DonkeyAnimator == null)
            DonkeyAnimator = GetComponent<Animator>();
        else if (DonkeyAnimator == null)
            DonkeyAnimator = GetComponentInChildren<Animator>();

        if (RigBuilder == null)
            RigBuilder = GetComponent<RigBuilder>();
        else if (DonkeyAnimator == null)
            RigBuilder = GetComponentInChildren<RigBuilder>();

        time = 0;
    }

    private void Deceleration()
    {
        RuntimeManager.PlayOneShot(DecelerateEvent);
    }

    private void Acceleration()
    {
        RuntimeManager.PlayOneShot(AccelerateEvent);
    }

    private void DonkeyStop()
    {
        RuntimeManager.PlayOneShot(DonkeyStopEvent);
    }

    private void VegetalSpell()
    {
        RuntimeManager.PlayOneShot(VegetalSpellEvent);

        ThiziriAnimator.SetTrigger("VegetalSpell");
        DonkeyAnimator.SetTrigger("VegetalSpell");
    }

    private void LightSpell()
    {
        RuntimeManager.PlayOneShot(LightSpellEvent);
        //TODO
        ThiziriAnimator.SetTrigger("LightSpell");
        DonkeyAnimator.SetTrigger("LightSpell");
    }

    private void Update()
    {
        time += Time.deltaTime;
        ThiziriAnimator.SetFloat("Time", time);
        DonkeyAnimator.SetFloat("Time", time);
    }

    private void LateUpdate()
    {
        MoveForward();
        Rotate();
    }

    private void Rotate()
    {
        var rotation = playerControl.Rotation;

        DonkeyAnimator.SetFloat("Rotation", rotation);
        DonkeyAnimator.SetLayerWeight(2, Mathf.Abs(rotation));
        ThiziriAnimator.SetFloat("Rotation", rotation);
        ThiziriAnimator.SetLayerWeight(2, Mathf.Abs(rotation));


        //Interpolate camera rotation with curve
        float rotationCamera = 0;
        var sign = Mathf.Sign(rotation);
        switch (sign)
        {
            case -1:
                rotationCamera = -1f * RotationCamera.Evaluate(Mathf.Abs(rotation)) * LeftMaxAngle;
                break;
            case 1:
                rotationCamera = RotationCamera.Evaluate(Mathf.Abs(rotation)) * RightMaxAngle;
                break;
        }

        //Rotate camera
        LookDirection.localRotation = Quaternion.AngleAxis(rotationCamera, Vector3.up);
    }

    private void MoveForward()
    {
        var speed = playerControl.Speed;
        DonkeyAnimator.SetFloat("Speed", speed);
        ThiziriAnimator.SetFloat("Speed", speed);
    }
}