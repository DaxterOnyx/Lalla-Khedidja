using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class FoliageScript : MonoBehaviour
{
    public Material[] materials;
    public Transform playerTransform;
    Vector3 _lastPosition = Vector3.zero;

    private void Update()
    {
        var position = playerTransform.transform.position;
        if (position != _lastPosition)
        {
            foreach (var m in materials)
            {
                m.SetVector("_position", position);
            }

            _lastPosition = position;
        }
    }
}