using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Fmod/Audio Manager", fileName = "Audio Manager")]
public class FMODAudioManagerCreator : ScriptableObject
{
    [Header("CHARACTER")]
  
    [FMODUnity.EventRef]
    public string footsteps = null;
    [FMODUnity.EventRef]
    public string walkFeedback = null;
    [FMODUnity.EventRef]
    public string trotFeedback = null;
    [FMODUnity.EventRef]
    public string gallopFeedback = null;

    [Header("SPELLS")]

    [FMODUnity.EventRef]
    public string vegetationSpellUsefull = null;
    [FMODUnity.EventRef]
    public string vegetationSpellUnusefull = null;
    [FMODUnity.EventRef]
    public string lightSpell = null;
    [FMODUnity.EventRef]
    public string notPossible = null;

    [Header("AMBIANCE")]

    [FMODUnity.EventRef]
    public string bed = null;
    [FMODUnity.EventRef]
    public string river = null;
    [FMODUnity.EventRef]
    public string bridgeGrow = null;
    [FMODUnity.EventRef]
    public string nearAshy = null;
    [FMODUnity.EventRef]
    public string nearOwl = null;
    [FMODUnity.EventRef]
    public string nearRaven = null;

    [Header("MUSIC")]

    [FMODUnity.EventRef]
    public string tutorial = null;
    [FMODUnity.EventRef]
    public string exploration = null;
    [FMODUnity.EventRef]
    public string ogress = null;

    [Header("UTILITIES")]

    [FMODUnity.EventRef]
    public string intoForest = null;
    [FMODUnity.EventRef]
    public string outOfForest = null;


}