﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FMODParameterTrigger3D : MonoBehaviour
{
    // Script à attacher à un collider dans le but de faire varier la valeur d'un paramètre
    
    // Valeur du paramètre une fois dans la zonne de collision
    public float enterValue = 1f;

    // Valeur du paramètre une fois sorti de la zone
    public float exitValue = 0f;

    // Référence du paramètre dans fmod
    [FMODUnity.ParamRef]
    public string paramPath;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            FMODUnity.RuntimeManager.StudioSystem.setParameterByName(paramPath, enterValue);
            Debug.Log("nearRiver activated");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            FMODUnity.RuntimeManager.StudioSystem.setParameterByName(paramPath, exitValue);
            Debug.Log("nearRiver deactivated");
        }
    }
}
