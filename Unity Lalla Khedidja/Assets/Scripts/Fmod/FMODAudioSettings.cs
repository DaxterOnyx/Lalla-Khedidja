﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Ce script permet de gérer trois sliders de volumes
//Et une fonction pour réinitialiser les niveaux

public class FMODAudioSettings : MonoBehaviour
{
    private static FMODAudioSettings instance = null;
    public static FMODAudioSettings sharedInstance
    {

        //Accesseur automatique
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<FMODAudioSettings>();
            }
            return instance;
        }
    }

    //On utilise des VCA plutôt que des bus pour deux raisons
    //1: pouvoir controler plusieurs bus avec un VCA sans modifier la hiérarchie du mixer
    //2: pouvoir gérer indépendamment le volume souhaité par le joueur et les volumes gérés par le mixeur dynamique (avec les transitions et autres par exemple)

    //Chemin d acces aux vca dans la hierarchie du projet FMOD
    public string masterVCAPath = "vca:/Master";
    public string musicVCAPath = "vca:/Music";
    public string sfxVCAPath = "vca:/SFX";

    private FMOD.Studio.VCA masterVCA;
    private FMOD.Studio.VCA musicVCA;
    private FMOD.Studio.VCA sfxVCA;

    //Volumes de depart
    public float masterStartVol = 1f;
    public float musicStartVol = 1f;
    public float sfxStartVol = 1f;

    //Sliders gerant ces volumes dans le menu
    public Slider masterVolSlider;
    public Slider musicVolSlider;
    public Slider sfxVolSlider;

    private void Start()
    {
        masterVCA = FMODUnity.RuntimeManager.GetVCA(masterVCAPath);
        musicVCA = FMODUnity.RuntimeManager.GetVCA(musicVCAPath);
        sfxVCA = FMODUnity.RuntimeManager.GetVCA(sfxVCAPath);

        float masterVol;
        masterVCA.getVolume(out masterVol);
        masterVolSlider.value = masterVol;

        float musicVol;
        masterVCA.getVolume(out musicVol);
        masterVolSlider.value = musicVol;

        float sfxVol;
        masterVCA.getVolume(out sfxVol);
        masterVolSlider.value = sfxVol;
    }

    // Fonctions a appeler (EN MODE DYNAMIC FLOAT) dans le slider
    public void SetMasterVol(float value)
    {
        masterVCA.setVolume(value);
    }

    public void SetMusicVol(float value)
    {
        musicVCA.setVolume(value);
    }

    public void SetSFXVol(float value)
    {
        sfxVCA.setVolume(value);
    }

    // Fonction a appeler sur un bouton pour remettre a zero les sliders de volume
    public void ResetVolumes()
    {
        // verifie qu'il y a bien un slider référencé pour chaque catégorie, sinon ne fait rien
        if(masterVolSlider != null)
        {
            masterVolSlider.value = masterStartVol;
        }

        if (musicVolSlider != null)
        {
            musicVolSlider.value = musicStartVol;
        }

        if (sfxVolSlider != null)
        {
            sfxVolSlider.value = sfxStartVol;
        }
    }
}
