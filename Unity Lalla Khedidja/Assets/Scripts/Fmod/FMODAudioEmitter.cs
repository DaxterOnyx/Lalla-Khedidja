﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Ce script permet de jouer un son 3D à partir d'un objet placé dans l'espace
//Il limite l'utilisation de cpu en stopant l'event si le joueur sort de la distance maximale d'atténuation

//Le script nécéssite pour cela un Sphere Collider pour détecter la présence du joueur
//Il faut régler le radius/rayon de la sphère
//pour qu'il soit EGAL À LA DISTANCE D'ATTENUATION MAXIMALE DU SON 3D
[RequireComponent(typeof(SphereCollider))]
[RequireComponent(typeof(Rigidbody))]
public class FMODAudioEmitter : MonoBehaviour
{
    //Event à jouer
    [FMODUnity.EventRef]
    public string eventPath;

    public FMOD.Studio.EventInstance audioEvent;

    //permet de savoir si le personnage est dans la zone d'effet
    private bool isInCollider = false;

    void Start()
    {
        //Afin de pouvoir en modifier des paramètres plus tard
        //Il faut créer une instance de l'event avant de le jouer
        audioEvent = FMODUnity.RuntimeManager.CreateInstance(eventPath);

        //Pour que le son puisse être placé dans l'espace 3D
        //et même pour qu'il puisse suivre un objet mouvant si besoin
        //il faut attacher l'instance d'event à un GameObject
        //Ici par son transform et un rigidbody
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(audioEvent, GetComponent<Transform>(), GetComponent<Rigidbody>());
    }

    private void OnTriggerEnter(Collider other)
    {
        //Si c'est le personnage joueur qui rentre dans la sphère d'effet, lance le son
        if (other.CompareTag("Player") && !isInCollider)
        {
            audioEvent.start();
            isInCollider = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //Si c'est le personnage joueur qui sort de la sphère d'effet, coupe le son
        if (other.CompareTag("Player") && isInCollider)
        {
            audioEvent.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            isInCollider = false;
        }
    }
}
