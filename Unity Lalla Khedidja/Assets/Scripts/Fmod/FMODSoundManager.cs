﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FMODSoundManager : MonoBehaviour
{
    private static FMODSoundManager instance = null;
    public static FMODSoundManager sharedInstance
    {

        //Accesseur automatique
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<FMODSoundManager>();
            }
            return instance;
        }
    }

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    [FMODUnity.EventRef]
    public string genericClickSound;

    [FMODUnity.EventRef]
    public string genericHoverSound;

    // Ce script est plutot vide, mais c'est parce qu'il sert principalement à gérer les spécificités de votre système sonore
}
